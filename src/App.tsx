import React, { useState } from "react";
import "./App.css";
import { Status } from "./types";

function App() {
  const [status, setStatus] = useState<Status | "Bienvenida">("Bienvenida");
  const [selected, setSelected] = useState<Status | null>(null);

  const renderBienvenida = () => {
    return (
      <div>
        <h1> Bienvenido.Esto es un ejercicio mental sobre la adultez.</h1>
        <br />
        <button onClick={() => setStatus("Inicio")}>Empezar</button>
      </div>
    );
  };

  const renderConfirmacion = (status: Status) => {
    return (
      <div>
        <h3>¿Realmente quieres empezar por ahí?</h3>

        <button
          onClick={() => {
            setSelected(null);
            setStatus(status);
          }}
        >
          Si
        </button>
        <button onClick={() => setSelected(null)}>No</button>
      </div>
    );
  };

  const renderInicio = () => {
    return (
      <div>
        <h3>¿Dónde quieres comenzar?</h3>

        <button onClick={() => setSelected("Libertad")}>Libertad</button>
        <button onClick={() => setSelected("Cuestionamientos")}>
          Cuestionamientos
        </button>
        <button onClick={() => setSelected("Presente")}>Presente</button>
        {selected && renderConfirmacion(selected)}
      </div>
    );
  };

  const renderLibertad = () => {
    return (
      <div>
        <h3>¿Algo de felicidad? ¿Qué prefieres?</h3>

        <button onClick={() => setStatus("Relaciones")}>Relaciones</button>
        <button onClick={() => setStatus("Decisiones")}>Decisiones</button>
        <button onClick={() => setStatus("Mente Plena")}>Mente Plena</button>
      </div>
    );
  };

  const renderCuestionamientos = () => {
    return (
      <div>
        <h3>¡Inconformidad! ¿Qué prefieres?</h3>
        <button onClick={() => setStatus("Mente Plena")}>Mente Plena</button>
        <button onClick={() => setStatus("Experimentación")}>
          Experimentación
        </button>
      </div>
    );
  };

  const renderPresente = () => {
    return (
      <div>
        <h3>¡Astucia! ¿Qué prefieres?</h3>
        <button onClick={() => setStatus("Experimentación")}>
          Experimentación
        </button>
        <button onClick={() => setStatus("Reinvención")}>Reinvención</button>
        <button onClick={() => setStatus("Incertidumbre")}>
          Incertidumbre
        </button>
      </div>
    );
  };

  const renderRelaciones = () => {
    return (
      <div>
        <h3>¿Ahora qué hacemos?</h3>
        <button onClick={() => setStatus("Reinvención")}>Reinvención</button>
        <button onClick={() => setStatus("Incertidumbre")}>
          Incertidumbre
        </button>
      </div>
    );
  };

  const renderDecisiones = () => {
    return (
      <div>
        <h3>¡Uy! ¿Qué queda ahora?</h3>
        <button onClick={() => setStatus("Cinismo")}>Reinvención</button>
        <button onClick={() => setStatus("Pasiones")}>Pasiones</button>
      </div>
    );
  };

  const renderMentePlena = () => {
    return (
      <div>
        <h3>¿Ahora qué hacemos?</h3>
        <button onClick={() => setStatus("Decisiones")}>Decisiones</button>
        <button onClick={() => setStatus("Pasiones")}>Pasiones</button>
        <button onClick={() => setStatus("Experimentación")}>
          Experimentación
        </button>
      </div>
    );
  };

  const renderReinvencion = () => {
    return (
      <div>
        <h3>¿A dónde te lleva?</h3>
        <button onClick={() => setStatus("Cinismo")}>Cinismo</button>
        <button onClick={() => setStatus("Decisiones")}>Decisiones</button>
      </div>
    );
  };

  const renderExperimentacion = () => {
    return (
      <div>
        <h3>¡Uy! ¿Qué queda ahora?</h3>
        <button onClick={() => setStatus("Pasiones")}>Pasiones</button>
        <button onClick={() => setStatus("Cinismo")}>Cinismo</button>
      </div>
    );
  };

  const renderImpasse = () => {
    return (
      <div>
        <h3>¡Impasse! Vuelve a empezar</h3>
        <button onClick={() => setStatus("Inicio")}>Entendido</button>
      </div>
    );
  };

  const renderIncertidumbre = () => {
    return (
      <div>
        <h3>¿Crees tener elección? Has acabado el juego.</h3>
      </div>
    );
  };

  const renderContenido = () => {
    console.log("rerendering");
    switch (status) {
      case "Bienvenida":
        return renderBienvenida();
      case "Inicio":
        return renderInicio();
      case "Libertad":
        return renderLibertad();
      case "Cuestionamientos":
        return renderCuestionamientos();
      case "Presente":
        return renderPresente();
      case "Relaciones":
        return renderRelaciones();
      case "Decisiones":
        return renderDecisiones();
      case "Mente Plena":
        return renderMentePlena();
      case "Reinvención":
        return renderReinvencion();
      case "Cinismo":
        return renderImpasse();
      case "Pasiones":
        return renderImpasse();
      case "Experimentación":
        return renderExperimentacion();
      case "Incertidumbre":
        return renderIncertidumbre();
    }
  };

  return (
    <div className="App">
      <header className="App-header">{renderContenido()}</header>
    </div>
  );
}

export default App;
