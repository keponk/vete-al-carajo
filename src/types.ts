export type Status =
  | "Inicio"
  | "Libertad"
  | "Decisiones"
  | "Relaciones"
  | "Cinismo"
  | "Cuestionamientos"
  | "Mente Plena"
  | "Pasiones"
  | "Experimentación"
  | "Presente"
  | "Reinvención"
  | "Incertidumbre";

const codeToString = Object.freeze({
  100: "HTTP 100 Continue",
  102: "HTTP 102 Processing",
  201: "HTTP 201 Created",
  207: "HTTP 207 Multi-Status",
  301: "HTTP 301 Moved Permanently",
  404: "HTTP 404 Not Found",
  408: "HTTP 408 Request Timeout",
  417: "HTTP 417 Expectation Failed",
  426: "HTTP 426 Upgrade Required",
});

type StatusToCode = {
  [status in Status]: keyof typeof codeToString;
};

const statusToCode: StatusToCode = Object.freeze({
  Inicio: 100,
  Libertad: 207,
  Decisiones: 201,
  Relaciones: 404,
  Cinismo: 417,
  Cuestionamientos: 102,
  "Mente Plena": 201,
  Pasiones: 408,
  Experimentación: 408,
  Presente: 301,
  Reinvención: 426,
  Incertidumbre: 301,
});

type StatusChoice = {
  title: Status;
  description: string;
  value: Status;
};

export const generateChoices = (statuses: Status[]): StatusChoice[] =>
  statuses.map((s) => ({
    title: s,
    description: codeToString[statusToCode[s]],
    value: s,
  }));
